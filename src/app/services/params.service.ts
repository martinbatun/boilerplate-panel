import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ActivatedRoute, Params } from '@angular/router';
import { HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ParamsService {

  get = new BehaviorSubject(null);
  params: HttpParams;

  constructor(
    private aR: ActivatedRoute,
  ) {
    this.aR.queryParams.subscribe(
      (params: Params) => {
        console.log(params);

        let prms: HttpParams = new HttpParams()
        Object.keys(params).forEach(function(key) {
          prms = prms.append(key, params[key]);
        });
        this.params = prms;
        this.get.next(true);
      }
    );
  }
}
