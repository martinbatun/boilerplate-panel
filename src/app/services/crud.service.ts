import { Injectable, EventEmitter } from '@angular/core';
import { ApiService } from './api.service';
import { Pagination } from '@sha/mat-table/table.interface';
import Swal from 'sweetalert2';
declare var $: any;

export interface Data {
  data: any[];
  pagination: Pagination
}
export class Respose {
  item: any;
  type: type;
}
type type = 'add' | 'edit' | 'delete';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  item = new EventEmitter<any>(); //evento emitido para el formulario para pasar el item seleccionado
  submit = new EventEmitter<boolean>(); //evento emitido del modal para el formulario para enviar el formulario
  action = new EventEmitter<Respose>(); //evento emitido para la tabla cuando se agrega, edita o elimina

  constructor(
    private API: ApiService
  ) { }

  // funcion para enviar evento con el item que se selecciona en la tabla
  select(e: any): void {
    console.log(e);
    this.item.next(e);
  }

  // funcion para regresar la respuesta despues de agregar o editar
  response(data: any, type: type, msg: string): void {
    let msg2 = type === 'add' ? 'agregado.' : 'editado.'
    this.msg(`${msg} ${msg2}`, true);
    this.action.next({ item: data, type: type });
  }

  // funcion para manipular los datos listados en la tabla
  actions(data: Data, item: Object, action: type): Data {
    if (action === 'add') {
      data.data.push(item);
    } else if (action === 'edit') {
      for (var i = 0; i < data.data.length; i++) {
        if (data.data[i].id === item['id']) { data.data[i] = item }
      }
    } else if (action === 'delete') {
      data.data = data.data.filter(elem => elem.id != item['id']);
    }

    data = {
      data: data.data.map(function(e) { return e; }),
      pagination: {
        total_rows: data.data.length,
        per_page: data.pagination.per_page,
        current_page: data.pagination.current_page,
        links: data.pagination.links
      }
    }

    return data;
  }

  // funcion para eliminar
  delete(url: string, item: any): void {
    console.log(url);
    Swal.fire({
      title: '¿Estas seguro?',
      text: "¡No podrás revertir esto!",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, bórralo!',
      cancelButtonText: 'No, cancelar!',
      customClass: { confirmButton: 'btn btn-sm btn-primary', cancelButton: 'btn btn-sm btn-secondary' },
    }).then((result) => {
      if (result.value) {
        this.API.delete(`${url}/${item.id}/`).subscribe(() => {
          this.action.next({ item: item, type: 'delete' })
          this.msg('Eliminado correctamente')
        })
      }
    })
  }

  // funcion para mostrar msg toast de confirmacion de evento
  msg(msg: string, close?: boolean): void {
    Swal.fire({
      toast: true,
      position: 'top-end',
      type: 'success',
      title: msg,
      showConfirmButton: false,
      timer: 1500
    });
    close ? this.closeModal() : '';
  }

  // funcion para cerrar el modal
  closeModal(): void {
    if ($("#componentsModal").is(":visible")) {
      $('#componentsModal').modal('toggle');
    }
  }

}
