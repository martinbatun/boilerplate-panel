import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import * as jwt_decode from 'jwt-decode';

export class User {
  id: number;
  name: string;
  email: string;
  rol: number;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  update = new BehaviorSubject(null);

  constructor(
    private r: Router
  ) { }

  set(data: { access: string }): void {
    localStorage.setItem('boilerplate-panel', this.utf8_to_b64(data['access']));
    // this.setUser(jwt_decode(data.access));
    setTimeout(() => { this.r.navigate(["sliders"]) }, 500);
  }

  get() {
    return localStorage.getItem("boilerplate-panel") ? jwt_decode(this.b64_to_utf8(localStorage.getItem("boilerplate-panel"))) : null;
  }
  getToken(): string {
    return localStorage.getItem("boilerplate-panel") ? this.b64_to_utf8(localStorage.getItem("boilerplate-panel")) : null;
  }

  setUser(user: User) {
    // localStorage.setItem('boilerplate-panel-user', this.utf8_to_b64(JSON.stringify(user)));
    this.update.next(true);
  }

  getUser(): User {
    return localStorage.getItem("boilerplate-panel-user") ? JSON.parse(this.b64_to_utf8(localStorage.getItem("boilerplate-panel-user"))) : null;
  }

  utf8_to_b64(str: string): string {
    return window.btoa(unescape(encodeURIComponent(str)));
  }

  b64_to_utf8(str: string): string {
    return decodeURIComponent(escape(window.atob(str)));
  }

  logout(): void {
    localStorage.removeItem('boilerplate-panel');
    // localStorage.removeItem('boilerplate-panel-user');
    // this.update.next(true);
    this.r.navigate(['/login']);
  }
}
