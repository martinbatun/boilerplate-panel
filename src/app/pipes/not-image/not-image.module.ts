import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotImagePipe } from './not-image.pipe';

@NgModule({
  declarations: [NotImagePipe],
  imports: [CommonModule],
  exports: [NotImagePipe]
})
export class NotImageModule { }
