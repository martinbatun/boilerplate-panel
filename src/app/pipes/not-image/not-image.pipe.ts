import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'notImage'
})
export class NotImagePipe implements PipeTransform {

  transform(image: string): string {
    // return image ? image : 'https://marvin.com.mx/wp-content/themes/15zine/library/images/placeholders/placeholder-378x300@2x.png';
    return image ? image : 'assets/logo.png';
  }

}
