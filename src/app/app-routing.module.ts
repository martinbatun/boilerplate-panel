import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@auth/auth.guard'
import { DashboardGuard } from '@dash/dashboard.guard';
import { DashboardComponent } from '@dash/dashboard.component';


const routes: Routes = [
  { path: '', loadChildren: () => import('@auth/auth.module').then(m => m.AuthModule), canActivate: [AuthGuard] },
  {
    path: '', component: DashboardComponent, canActivate: [DashboardGuard],
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'sliders' },
      {
        path: 'sliders',
        loadChildren: () => import('@dash/sliders/sliders.module').then(m => m.SlidersModule)
      },
      {
        path: 'blogs',
        loadChildren: () => import('@dash/blogs/blogs.module').then(m => m.BlogsModule)
      },
      {
        path: 'doctors',
        loadChildren: () => import('@dash/doctors/doctors.module').then(m => m.DoctorsModule)
      }
    ]
  },
  { path: '**', pathMatch: 'full', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
