import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'youtube',
  templateUrl: './youtube.component.html',
  styleUrls: ['./youtube.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class YoutubeComponent {

  @Input() src: string;

  constructor(private DS: DomSanitizer) { }

  getVideoIframe(url: string) {
    var video: string, results;

    if (url === null) {
      return '';
    }

    if(url.includes('//www.')){
      results = url.match('[\\?&]v=([^&#]*)');
      video = (results === null) ? url : results[1];
    } else {
      let urlArray = url.split('/');
      video = urlArray[urlArray.length - 1 ]
    }

    return this.DS.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + video);
  }

}
