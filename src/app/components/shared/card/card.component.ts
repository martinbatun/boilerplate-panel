import { Component, OnInit, Input } from '@angular/core';

type bar = 'top' | 'bottom';

@Component({
  selector: 'card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() color: string;
  @Input() img: string;
  @Input() title: string;
  @Input() description: string;
  @Input() width: number = 100;
  @Input() bar: bar = 'top';

  constructor() { }

  ngOnInit(): void {
  }

}
