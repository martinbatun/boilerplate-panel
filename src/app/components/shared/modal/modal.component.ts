import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { CrudService } from '@ser/crud.service';
declare var $: any;

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Input() submit = new EventEmitter<boolean>();
  @Input() add: boolean = true;

  constructor(
    public CRUD: CrudService
  ) { }

  ngOnInit() {
    // $('#componentsModal').modal({backdrop: false, keyboard: false})
  }
}
