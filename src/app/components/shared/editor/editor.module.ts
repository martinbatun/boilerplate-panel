import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditorComponent } from './editor.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular'
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [EditorComponent],
  imports: [
    CommonModule,
    FormsModule,
    CKEditorModule
  ],
  exports:[EditorComponent]
})
export class EditorModule { }
