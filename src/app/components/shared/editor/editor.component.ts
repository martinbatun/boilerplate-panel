import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular';

@Component({
  selector: 'editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {

  public Editor = ClassicEditor;
  config = {
    mediaEmbed: {
      previewsInData: true,
    }
  }
  @Input() data: string;
  @Output() output = new EventEmitter<string>();;

  constructor() { }

  public onChange({ editor }: ChangeEvent) {
    this.output.next(editor.getData())
    console.log(editor.getData());
  }

  ngOnInit(): void {
  }
}
