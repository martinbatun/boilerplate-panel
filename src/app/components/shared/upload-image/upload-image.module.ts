import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadImageComponent } from './upload-image.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [UploadImageComponent],
  imports: [
    CommonModule,
    ImageCropperModule,
    MatButtonModule
  ],
  exports: [UploadImageComponent]
})
export class UploadImageModule { }
