import { Component, Input, OnInit, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { ImageCroppedEvent, ImageTransform } from 'ngx-image-cropper';

@Component({
  selector: 'upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.scss']
})
export class UploadImageComponent implements OnChanges {

  @Input() width: number;
  @Input() img: string;
  @Input() round: boolean;
  @Input() aspectRatio: any = 1 / 1;
  @Input() resizeToWidth: number = 0;
  @Input() resizeToHeight: number = 0;
  // @Output() file: File = null;
  @Output() file = new EventEmitter<File>();

  imageChangedEvent: any = '';
  transform: ImageTransform = {};
  scale = 1;

  constructor() { }

  ngOnChanges(SC: SimpleChanges) {
    if (SC.img) {
      this.imageChangedEvent = null;
    }
  }

  fileChangeEvent(event: any): void {
    console.log(event);
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.img = event.base64;
    console.log(event);

    var t = this;
    (fetch(event.base64)
      .then(function(res) { return res.arrayBuffer(); })
      .then(function(buf) { return new File([buf], Date.now() + '.jpeg', { type: 'image/jpeg' }); })
    ).then(function(file) {
      if (file) {
        console.log(file);
        t.file.next(file);

        // dis.value = file;
        // dis.onFileSelect.emit(file);
        // dis.fileObj = file;
        // if (file.type.match(/image\/*/) != null) {
        //   var reader = new FileReader();
        //   // dis.imgDataURL = file;
        //   reader.readAsDataURL(file);
        //   reader.onload = (_event) => {
        //     dis.imgDataURL = dis.sanitizedDataURL(reader.result);
        //   }
        // }
        // dis.displayResizeWindow = false;
      }
    });
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }

  zoomOut(): void {
    this.scale -= .1;
    this.transform = {
      ...this.transform,
      scale: this.scale
    };
  }

  zoomIn(): void {
    this.scale += .1;
    this.transform = {
      ...this.transform,
      scale: this.scale
    };
  }

}
