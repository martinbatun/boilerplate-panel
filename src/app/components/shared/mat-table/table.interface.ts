export interface Links {
  first: string;
  last: string;
  next: string;
  prev: string;
}

export interface Pagination {
  total_rows: number;
  per_page: number;
  current_page: number;
  links: Links;
}

export interface PaginatorConfig {
  length: number;
  pageSize: number;
  pageIndex: number;
  pageSizeOptions: Array<number>;
}

export class PageConfig {
  length: number;
  pageSize: number;
  pageIndex: number;
  pageSizeOptions: Array<number>;

  constructor(pagination: Pagination) {
    this.length = pagination.total_rows;
    this.pageSize = Number(pagination.per_page);
    this.pageIndex = pagination.current_page;
    this.pageSizeOptions = [5, 10, 25, 100];
  }
}

export class DataResponse {
  data: any[];
  pagination: Pagination;
}
