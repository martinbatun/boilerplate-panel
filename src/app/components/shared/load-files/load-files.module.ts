import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadFilesComponent } from './load-files.component';
import { SvgModule } from '@sha/svg/svg.module';

@NgModule({
  declarations: [LoadFilesComponent],
  imports: [CommonModule, SvgModule],
  exports: [LoadFilesComponent]
})
export class LoadFilesModule { }
