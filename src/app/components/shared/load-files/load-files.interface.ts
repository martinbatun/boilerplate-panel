export class FileItem {
  file: File;
  src: string;
  name: string;
  opacity: number = 0;
  uploading: boolean = false;
  upload_percentage: number = 0;

  constructor(archivo: any, base_url?: string) {

    this.file = archivo;
    this.name = archivo.name;

    if (archivo.type != undefined) {
      let fileReader = new FileReader();
      fileReader.onload = (e: any) => {
        this.src = e.target.result;
      }
      fileReader.readAsDataURL(archivo);
    } else {
      this.name = archivo.file
      this.src = base_url + archivo.file;
    }
  }
}
