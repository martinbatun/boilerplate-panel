import { Component, OnChanges, Input, SimpleChanges, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from '@ser/api.service';
import { NoContent } from '@ani/animations';
import { FileItem } from './load-files.interface';
import Swal from 'sweetalert2';


@Component({
  selector: 'load-files',
  templateUrl: './load-files.component.html',
  styleUrls: ['./load-files.component.css'],
  animations: [NoContent]
})
export class LoadFilesComponent implements OnChanges {

  @ViewChild('loadEvidence') loadEvidence: ElementRef;

  @Input() accept: string;
  @Input() url: string;
  @Input() base_url: string;
  @Input() filesArray: any;
  @Output() file = new EventEmitter<{ file: string }>();
  @Output() remove = new EventEmitter<string>();

  files: FileItem[] = [];

  constructor(public api: ApiService) { }

  ngOnChanges(simpleChanges: SimpleChanges): void {
    console.log(simpleChanges);

    if (this.base_url) {
      if (simpleChanges.filesArray && simpleChanges.filesArray.currentValue) {
        this.files = [];
        simpleChanges.filesArray.currentValue.forEach(element => {
          this.files.push(new FileItem(element, this.base_url));
        });
      }
    }
  }

  cargandoImagen(files: FileList): void {
    for (const propiedad in Object.getOwnPropertyNames(files)) {
      const archivo = new FileItem(files[propiedad]);
      if (archivo.file.size > 0) {
        this.upload(archivo);
      }
    }
  }

  upload(file: FileItem): void {
    const formData = new FormData();
    formData.append('file', file.file, file.file.name);

    this.api.post(`${this.url}/file/`, formData).subscribe((data: { file: string }) => {
      this.files.push(file)
      this.file.emit(data);
      file.name = data.file;
      file.opacity = 1;
      file.uploading = true;
      for (let i = 1; i <= 100; i++) {
        setTimeout(() => {
          file.upload_percentage = i;
          if (i == 100) {
            this.complete(file);
          }
        }, 200);
      }
    });
  }

  complete(file: FileItem): void {
    setTimeout(() => {
      file.uploading = false;
      for (let i = 1; i <= 10; i++) {
        setTimeout(() => {
          file.opacity = 10 - i;
        }, 700);
      }
    }, 500);
  }

  delete(name: string): void {
    Swal.fire({
      title: 'Eliminar imagen',
      text: '¿Desea eliminar esta imagen?',
      type: 'question',
      showCancelButton: true,
      confirmButtonText: 'SI',
      cancelButtonText: 'NO',
      customClass: { confirmButton: 'btn btn-sm btn-primary', cancelButton: 'btn btn-sm btn-secondary' },
    }).then((result) => {
      if (result.value) {
        this.api.delete(`${this.url}/file/${name}/`).subscribe(() => {
          remove();
        }, err => {
          if (err.status === 400) {
            remove();
          }
        });
      }
    });

    let t = this;
    function remove() {
      t.files = t.files.filter(elem => elem.name != name);
      t.remove.emit(name);
    }
  }
}
