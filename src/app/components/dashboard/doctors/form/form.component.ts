import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CrudService } from '@ser/crud.service';
import { ApiService } from '@ser/api.service';
import { Doctor } from '../doctors.interface';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent {

  form: FormGroup;
  hide: boolean = true;

  constructor(
    private FB: FormBuilder,
    private CRUD: CrudService,
    private API: ApiService
  ) {
    this.CRUD.submit.subscribe(() => { this.onSubmit() });
    this.CRUD.item.subscribe((data: Doctor) => {
      this.form.reset();
      if (data) {
        this.form.patchValue(data)
        this.form.removeControl('password');
        this.form.removeControl('confirm_password');
        this.form.updateValueAndValidity();
      }
    });

    this.form = this.FB.group({
      id: [null],
      img: [],
      img_file: [],
      full_name: ['martin batun', [Validators.required]],
      phone: ['999-999-9999', [Validators.required, Validators.pattern("[0-9]{3}-[0-9]{3}-[0-9]{4}")]],
      email: [, [Validators.email, Validators.required]],
      password: [, Validators.required],
      confirm_password: [, Validators.required]
    });
  }

  get f() { return this.form.controls; }
  onSubmit(): void {
    if (this.form.invalid) { return }

    const val = this.form.value;
    const data = new FormData()
    data.append('full_name', val.full_name)
    data.append('phone', val.phone)
    data.append('email', val.email)
    if (val.img_file) { data.append('img', val.img_file, val.img_file.name) }

    // si no tiene id agregara un nuevo registro de lo contrario aria una edicion
    const msg = 'Doctor'
    if (!val.id) {
      data.append('password', val.password)
      data.append('confirm_password', val.confirm_password)
      this.API.post('users/', data).subscribe((data: Doctor) => {
        this.CRUD.response(data, 'add', msg)
      });
    } else {
      this.API.patch(`users/${val.id}/`, data).subscribe((data: Doctor) => {
        this.CRUD.response(data, 'edit', msg)
      });
    }
  }

}
