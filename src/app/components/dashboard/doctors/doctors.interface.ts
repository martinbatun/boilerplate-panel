import { Pagination } from '@sha/mat-table/table.interface';

export interface Doctor {
  id: number;
  full_name: string;
  phone: string;
  email: string;
}

export interface Data {
  data: Doctor[];
  pagination: Pagination
}
