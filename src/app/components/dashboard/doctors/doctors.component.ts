import { Component } from '@angular/core';
import { ApiService } from '@ser/api.service';
import { ParamsService } from '@ser/params.service';
import { CrudService, Respose } from '@ser/crud.service';
import { Data } from './doctors.interface';


@Component({
  selector: 'app-doctors',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.css']
})
export class DoctorsComponent {

  data: Data;
  columns: string[] = ['name', 'email', 'phone', 'actions'];

  constructor(
    private API: ApiService,
    private PRMS: ParamsService,
    public CRUD: CrudService
  ) {
    this.CRUD.action.subscribe((e: Respose) => {
      this.data = this.CRUD.actions(this.data, e.item, e.type);
    });

    this.PRMS.get.subscribe(() => {
      this.API.get(`user/`, this.PRMS.params).subscribe((data: Data) => { this.data = data });
    });
  }
}
