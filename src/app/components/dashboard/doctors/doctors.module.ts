import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoctorsComponent } from './doctors.component';
import { RouterModule } from '@angular/router';
import { TableModule } from '@sha/mat-table/table.module';
import { ModalModule } from '@sha/modal/modal.module';
import { FormModule } from './form/form.module';


@NgModule({
  declarations: [DoctorsComponent],
  imports: [
    RouterModule.forChild([{ path: '', component: DoctorsComponent }]),
    CommonModule,
    TableModule,
    ModalModule,
    FormModule
  ]
})
export class DoctorsModule { }
