import { Component, OnInit, HostListener, HostBinding } from '@angular/core';
import { Router, Event, NavigationEnd } from "@angular/router";
import { ApiService } from '@ser/api.service';
import { LoaderService } from '@ser/loader.service';
import { UserService, User } from '@ser/user.service';
import { MenuItems, MENUITEMS, ERole } from './dashboard.config';
import { PageAnimations, ListStagger } from '@ani/animations';
declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  animations: [PageAnimations, ListStagger],
})
export class DashboardComponent implements OnInit {
  @HostBinding('@pageAnimations')

  toggled: boolean;
  pinned: boolean;
  hovered: boolean;
  user: User;

  menuItems: MenuItems[] = [];

  constructor(
    private RT: Router,
    private USER: UserService,
    private API: ApiService,
    private LS: LoaderService,
  ) {
    this.RT.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.LS.start(); this.LS.stop(1000);
      }
    });

    this.USER.update.subscribe(() => {
      this.user = this.USER.getUser();
      console.log(this.user);
    });

    // this.menuItems = MENUITEMS.filter(x => { return x.role.includes(this.user.rol) ? x : null });
    this.menuItems = MENUITEMS.filter(x => { return x });

  }

  ngOnInit() {
    this.resize();
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.resize();
  }

  resize(): void {
    if (window.innerWidth < 992) {
      this.toggled = false;
      if ($('.overlay').hasClass('active')) {
        $('.overlay').removeClass('active');
      }
    } else {
      this.toggled = true;
      $('.overlay').removeClass('active');
    }
  }

  collapse(boolean: boolean): void {
    if (window.innerWidth < 992) {
      if (this.toggled) {
        this.toggled = false;
        $('.overlay').removeClass('active');
      } else {
        this.toggled = true;
        $('.overlay').addClass('active');
      }
    } else if (window.innerWidth > 992 && boolean) {
      this.toggled = !this.toggled;
    }
  }

  logout(): void {
    // this.API.post('login/logout', {}).subscribe((data: { mensajes: string }) => {
    this.USER.logout();
    // });
  }
}
