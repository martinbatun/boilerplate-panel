import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { CardModule } from '@sha/card/card.module';
import { ModalModule } from '@sha/modal/modal.module';
import { BlogsComponent } from './blogs.component';
import { FormModule } from './form/form.module';


@NgModule({
  declarations: [BlogsComponent],
  imports: [
    RouterModule.forChild([{ path: '', component: BlogsComponent }]),
    CommonModule,
    CardModule,
    MatButtonModule,
    ModalModule,
    FormModule
  ]
})
export class BlogsModule { }
