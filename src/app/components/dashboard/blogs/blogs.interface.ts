import { Pagination } from '@sha/mat-table/table.interface';

export interface Picture {
  src: string;
  type: 'image' | 'video';
  align: 'right' | 'left';
}

export interface BlogBody {
  p: string;
  picture?: Picture;
}

export interface Blog {
  id: number;
  title: string;
  description: string;
  date: string;
  color: string;
  body: BlogBody[];
}

export interface Data {
  data: Blog[];
  pagination: Pagination
}
