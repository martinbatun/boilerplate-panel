import { Component, OnInit } from '@angular/core';
import { CrudService, Respose } from '@ser/crud.service';
import { ApiService } from '@ser/api.service';
import { ParamsService } from '@ser/params.service';
import { Data } from './blogs.interface';


@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {

  data: Data;

  constructor(
    private API: ApiService,
    private PRMS: ParamsService,
    public CRUD: CrudService
  ) {
    this.CRUD.action.subscribe((e: Respose) => {
      this.data = this.CRUD.actions(this.data, e.item, e.type);
    });

    this.PRMS.get.subscribe(() => {
      this.API.get(`blog/`, this.PRMS.params).subscribe((data: Data) => { this.data = data });
    });
  }

  ngOnInit(): void {
  }

}
