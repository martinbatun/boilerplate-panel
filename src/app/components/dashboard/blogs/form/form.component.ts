import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CrudService } from '@ser/crud.service';
import { ApiService } from '@ser/api.service';
import { Blog, BlogBody } from '../blogs.interface';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent {

  form: FormGroup;
  body: BlogBody[] = [];

  constructor(
    private FB: FormBuilder,
    private CRUD: CrudService,
    private API: ApiService
  ) {
    this.CRUD.submit.subscribe(() => { this.onSubmit() });
    this.CRUD.item.subscribe((data: Blog) => {
      this.form.reset();
      if (data) { this.form.patchValue(data); this.body = data.body; }
      else { this.form.patchValue({ color: "#000000" }) }
    });

    this.form = this.FB.group({
      id: [null],
      img: [],
      img_file: [],
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
      date: ['', [Validators.required]],
      color: [, Validators.required]
    });
  }

  /**
   * Contruye un nuevo parrafo.
   * @author Martin Batun Tec.
  */
  newParagraph(): void {
    this.body.push({
      p: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',
      picture: null
    })
  }

  /**
   * crea el base64 de una imagen y la agrega al parrafo.
   * @param file archivo cargado.
   * @param item parrafo en el cual sera agregado la imagen.
   * @author Martin Batun Tec.
  */
  upload(file: File, item: BlogBody): void {
    let fileReader = new FileReader();
    fileReader.onload = (e: any) => {
      let align = item.picture ? item.picture.align : 'right';
      item.picture = {
        src: e.target.result,
        type: 'image',
        align: align
      }
    }
    fileReader.readAsDataURL(file);
  }

  /**
   * Asigna el tipo youtube para poder cargar una url de youtube.
   * @param item parrafo en el cual sera asignado
   * @author Martin Batun Tec.
  */
  addUrlYoutube(item: BlogBody): void {
    item.picture = {
      src: '',
      type: 'video',
      align: item.picture ? item.picture.align : 'left'
    }
  }

  /**
   * Envia a guardar el blog.
   * @author Martin Batun Tec.
  */
  get f() { return this.form.controls; }
  onSubmit(): void {
    if (this.form.invalid) { return }

    const val = this.form.value;
    const data = new FormData()
    data.append('title', val.title)
    data.append('description', val.description)
    data.append('color', val.color)
    data.append('date', val.date)
    data.append('body', JSON.stringify(this.body))
    if (val.img_file) { data.append('img', val.img_file, val.img_file.name) }

    // si no tiene id agregara un nuevo registro de lo contrario aria una edicion
    const msg = 'Blog';
    if (!val.id) {
      this.API.post('blog/', data).subscribe((data: Blog) => {
        this.CRUD.response(data, 'add', msg)
      });
    } else {
      this.API.patch(`blog/${val.id}/`, data).subscribe((data: Blog) => {
        this.CRUD.response(data, 'edit', msg)
      });
    }
  }

}
