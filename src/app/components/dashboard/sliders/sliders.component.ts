import { Component, OnInit } from '@angular/core';
import { Data } from './sliders.interface';
import { ApiService } from '@ser/api.service';
import { ParamsService } from '@ser/params.service';
import { CrudService, Respose } from '@ser/crud.service';


@Component({
  selector: 'sliders',
  templateUrl: './sliders.component.html',
  styleUrls: ['./sliders.component.css']
})
export class SlidersComponent implements OnInit {

  data: Data;

  constructor(
    private API: ApiService,
    private PRMS: ParamsService,
    public CRUD: CrudService
  ) {
    this.CRUD.action.subscribe((e: Respose) => {
      this.data = this.CRUD.actions(this.data, e.item, e.type);
    });

    this.PRMS.get.subscribe(() => {
      this.API.get(`slider/`, this.PRMS.params).subscribe((data: Data) => { this.data = data });
    });
  }

  ngOnInit(): void {
  }

}
