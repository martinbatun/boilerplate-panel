import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CrudService } from '@ser/crud.service';
import { ApiService } from '@ser/api.service';
import { Slider } from '../sliders.interface';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent {

  form: FormGroup;

  constructor(
    private FB: FormBuilder,
    private CRUD: CrudService,
    private API: ApiService
  ) {
    this.CRUD.submit.subscribe(() => { this.onSubmit() });
    this.CRUD.item.subscribe((data: Slider) => {
      console.log(data);

      this.form.reset();
      if (data) { this.form.patchValue(data) }

      console.log(this.form);

    });

    this.form = this.FB.group({
      id: [null],
      img: [],
      img_file: [],
      img_device: [],
      img_device_file: []
    });
  }

  get f() { return this.form.controls; }
  onSubmit(): void {
    if (this.form.invalid) { return }

    const val = this.form.value;
    const data = new FormData()
    if (val.img_file) { data.append('img', val.img_file, val.img_file.name) }
    if (val.img_device_file) { data.append('img_device', val.img_device_file, val.img_device_file.name) }

    // si no tiene id agregara un nuevo registro de lo contrario aria una edicion
    const msg = 'Imagen'
    if (!val.id) {
      this.API.post('slider/', data).subscribe((data: Slider) => {
        this.CRUD.response(data, 'add', msg)
      });
    } else {
      this.API.patch(`slider/${val.id}/`, data).subscribe((data: Slider) => {
        this.CRUD.response(data, 'edit', msg)
      });
    }
  }

}
