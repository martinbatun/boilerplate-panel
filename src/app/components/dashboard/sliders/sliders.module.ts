import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { ModalModule } from '@sha/modal/modal.module';
import { SlidersComponent } from './sliders.component';
import { FormModule } from './form/form.module';


@NgModule({
  declarations: [SlidersComponent],
  imports: [
    RouterModule.forChild([{ path: '', component: SlidersComponent }]),
    CommonModule,
    MatButtonModule,
    ModalModule,
    FormModule
  ]
})
export class SlidersModule { }
