import { Pagination } from '@sha/mat-table/table.interface';

export interface Slider {
  id: number;
  img?: string;
  img_device?: string;
}

export interface Data {
  data: Slider[];
  pagination: Pagination
}
