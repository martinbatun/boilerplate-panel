import { Params } from '@angular/router';

export enum ERole {
  Admin = 2,
  Doctor = 3,
  Client = 4
}

export interface MenuItems {
  name: string;
  url?: string;
  prms?: Params;
  role?: ERole[];
  icon?: string;
  children?: MenuItems[];
  header?: boolean;
}

export const MENUITEMS: MenuItems[] = [
  {
    name: "Ordenes",
    children: null,
    url: "/orders",
    icon: "fas fa-list-ul",
    role: [ERole.Doctor]
  },
  {
    name: "Sliders",
    children: null,
    url: "/sliders",
    icon: "far fa-image",
    role: [ERole.Admin]
  },
  {
    name: "Sucursales",
    children: null,
    url: "/branchOffices",
    icon: "fas fa-store-alt",
    role: [ERole.Admin]
  },
  {
    name: "Promociones",
    children: null,
    url: "/promotions",
    icon: "fas fa-percent",
    role: [ERole.Admin]
  },
  {
    name: "Blog",
    children: null,
    url: "/blogs",
    icon: "fas fa-comment-alt",
    role: [ERole.Admin]
  },
  {
    name: "Pagos",
    children: null,
    url: "/payments",
    icon: "fas fa-hand-holding-usd",
    role: [ERole.Admin]
  },
  {
    name: "Médicos",
    children: null,
    url: "/doctors",
    icon: "fas fa-user-md",
    role: [ERole.Admin]
  }
]
