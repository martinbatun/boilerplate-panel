import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UserService } from '@ser/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private RT: Router, private US: UserService) { }
  canActivate() {
    if (!this.US.get()) {
      return true;
    } else {
      this.RT.navigate(['sliders']);
      return false;
    }
  }
}
