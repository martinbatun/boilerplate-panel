import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '@ser/api.service';
import { UserService } from '@ser/user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../auth.component.css']
})
export class LoginComponent implements OnInit {

  sub: Subscription;
  form: FormGroup;
  submitted: boolean = false;
  btnloader: boolean = false;

  constructor(
    private FB: FormBuilder,
    private API: ApiService,
    private USER: UserService
  ) { }

  ngOnInit(): void {
    this.form = this.FB.group({
      email: ['admin-laboratorio@yopmail.com', [Validators.required, Validators.email]],
      password: ['holamundo', [Validators.required]]
    });
  }

  get f() { return this.form.controls; }
  onSubmit(): void {
    this.submitted = true;
    // stop here if form is invalid
    if (this.form.invalid) { return; }
    this.btnloader = true;

    this.sub = this.API.post('token/', this.form.value).subscribe((data: any) => {
      this.USER.set(data);
    });

    this.sub.add(() => this.btnloader = false);
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
